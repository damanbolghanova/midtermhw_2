package main

import (
	"fmt"
	"sort"
	"strings"
	"sync"
)

func ExecutePipeline(jobs ...job) {
	in := make(chan interface{})
	out := make(chan interface{})

	wg := sync.WaitGroup{}
	wg.Add(1)

	for _, task := range jobs {
		go func(task job, in, out chan interface{}, wg *sync.WaitGroup) {
			defer wg.Done()
			task(in, out)
		}(task, in, out, &wg)
	}
	wg.Wait()

}

func SingleHash(in, out chan interface{}) {
	wg := &sync.WaitGroup{}
	wg.Add(1)
	for data := range in {
		go SingleHashWorker(data.(string), out, wg)
	}
	wg.Wait()
}

func SingleHashWorker(data string, out chan interface{}, wg *sync.WaitGroup) {
	defer wg.Done()

	var md5Data = DataSignerMd5(data)
	var crc32md5Data = DataSignerCrc32(md5Data)
	var crc32Data = DataSignerCrc32(data)

	result := crc32md5Data + "~" + crc32Data

	fmt.Println(data + " SingleHash data " + data)
	fmt.Println(data + " SingleHash md5(data) " + md5Data)
	fmt.Println(data + " SingleHash crc32(md5(data)) " + crc32md5Data)
	fmt.Println(data + " SingleHash crc32(data) " + crc32Data)
	fmt.Println(data + " SingleHash result " + result)

	out <- result

}
func MultiHash(in, out chan interface{}) {
	wg := &sync.WaitGroup{}
	for data := range in {
		wg.Add(1)
		go MultiHashWorker(data, out, wg)
	}

	wg.Wait()
}

func MultiHashWorker(data interface{}, out chan interface{}, wg *sync.WaitGroup) {
	defer wg.Done()

	for th := 0; th < 6; th++ {
		wg.Add(1)
		go func(data interface{}, out chan interface{}, wg *sync.WaitGroup) {
			//todo
		}(data, out, wg)
	}
	//out <-
	wg.Wait()
}

func CombineResults(in, out chan interface{}) {
	var firstResult []string
	for data := range in {
		firstResult = append(firstResult, data.(string))
	}
	sort.Strings(firstResult)
	finalResult := strings.Join(firstResult, "_")
	fmt.Println("Combine Results " + finalResult)
	out <- finalResult

}
